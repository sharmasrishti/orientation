# Git Summary


### What is Git ?

The way of managing code and a version control software that makes collabration with the teammates super simple is known as **GIT**.It is designed for coordinating work among programmers and even lets uou to keep the track of every revision during the development of the software.

Basically, Git is important for every programmer as it gives the hands-on experience.

Some of the goals of Git are as follows:
* speed
* data integrity
* support for distributed and non-linear workflows


### Git Vocabulary

##### Repository:

It is a file location where you are storing all the files related to your project. It is also known as **repo**.

##### GitLab:

It is the second most powerful and popular remote storage solution for git repository.

##### Commit:

It is command used to save changes to the local respository. 

##### Push:

This command is used to upload local repository content to remote repository.

##### Branch:

A branch is imply a lightweight movable pointer to one of the commits. The default branch name is **master**. 

##### Merge:

Merging is a way of putting forked history back together again. This command lets you take the independent lines of development created by git branch. 

##### Clone:

Git clone is primarily used to point to an existing repo and make a clone or copy of that repo at in a new directory, at another location.

##### Fork:

Forking is a lot like cloning, it allows you to freely experiment with changes without affecting the original project.


### How to install Git

For Linux, open the terminal and type: 

    sudo apt-get install git 

For Windows, [download the installer](https://git-scm.com/download/win) and run it. 

**Note:** Git only workks on Ubuntu. If not [there are various Linux package installation commands](https://git-scm.com/download/linux) 


### Git Internals

![Structure of Git Internals](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

### Git Workflow

![](https://1.bp.blogspot.com/-n8gwrM5Bf04/UfosDLuuDUI/AAAAAAAAKwg/2aE3V0NDk-g/s1600/git-and-github-workflow.png)

